// Obtener el boton
const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener('click', function(){
    let valorAuto=document.getElementById('txtValorAuto').value;
    let porcentaje = document.getElementById('txtPorcentaje').value;
    let plazo=document.getElementById('plazos').value;

    // Hacer los Calculos

    let pagoInicial = valorAuto * (porcentaje/100);
    let totalFin = valorAuto - pagoInicial;
    let plazos = totalFin/plazo;

    document.getElementById('txtPagoInicial').value = pagoInicial;
    document.getElementById('txtTotalFin').value = totalFin;
    document.getElementById('txtPagoMensual').value = plazos;
});

// Codificar el boton de limpiar

const btnLimpiar = document.getElementById('btnLimpiar');
btnLimpiar.addEventListener('click', function () {

    document.getElementById('txtValorAuto').value = '';
    document.getElementById('txtPorcentaje').value = '';
    document.getElementById('plazos').selectedIndex = '';


    document.getElementById('txtPagoInicial').value = '';
    document.getElementById('txtTotalFin').value = '';
    document.getElementById('txtPagoMensual').value = '';
});
