document.addEventListener('DOMContentLoaded', function() {
    ejecutarFunciones();
});

// Declaracion de arreglos

var arreglo = [19,20,3,2,6,3,3,9];

// Mostrar los elementos del arreglo

function mostrarArray(){
    for(con = 0; con < arreglo.length; ++con){
        console.log(con + ":" + arreglo[con]);
    } 
}

// Funcion que regresa el promedio de los elementos del arreglo

function promedio(){
    let promedio = 0;

    for(con=0; con< arreglo.length; ++con){
        promedio = promedio + arreglo[con];
    }

    promedio = promedio / arreglo.length;
    console.log(promedio);
}

// Realizar una funcion que regrese un valor entero que represente la cantidad de numeros pares en el arreglo

function pares() {
    for (con = 0; con < arreglo.length; ++con) {
        
        if (arreglo[con] % 2 == 0) {
            console.log("Si es par" + arreglo[con]);
        } else {
            console.log("No es par");
        }
    }
}

// Realiza una funcion que te regrese el valor mayor, contenido en array



function mayor(){
    let mayor = 0;

    for (con = 0; con < arreglo.length; ++con) {
        if (arreglo[con] >= mayor) {
            mayor = arreglo[con];
        }
    }

    console.log(mayor);
}


// Diseñe una funcion que resive un valor numerico que indica la cantidad de elementos del arreglo y el arreglo debera de llenarse con valores aleatorios en el rango del 0 al 1000
// Posteriormente mostrara el arreglo 

function random(num) {

    let p = document.getElementById('p');
    let p2 = document.getElementById('p2');
    let p3 = document.getElementById('p3');
    let p4 = document.getElementById('p4');
    let p5 = document.getElementById('p5');
    let p6 = document.getElementById('p6');

    const cmbNumeros = document.getElementById('cmbNumeros');
    cmbNumeros.innerHTML = "";
    let arr = []
    let a = 0
    let b = 0
    let array = num / 100
    let promedio = 0
    let ma = 0;
    let even = 0
    let odd = 0
    let me = 1000;

    for (let con = 0; con < num; ++con) {
        arr[con] = Math.floor(Math.random() * 1000)
        promedio += arr[con];
        let option = document.createElement('OPTION');
        option.value = arr[con];
        option.innerHTML = arr[con];
        cmbNumeros.appendChild(option);

        if (arr[con] % 2 == 0) {
            even += 1
        } else {
            odd += 1
        }

        if (arr[con] >= ma) {
            ma = arr[con];
            b = con;

        }
        if (arr[con] <= me) {
            me = arr[con];
            a = con;
        }


    }

    // Funcion isSimetrico, esta funcion recibe como argumento un arreglo con numeros aleatorios y me regresa el porcentaje de los valores

    let porcentajeEven = even / array;
    let porcentajeOdd = odd / array;
    let cons;

        if (porcentajeEven <= 20 || porcentajeOdd <= 20) {
            cons = "No";
        } else {
            cons = "Sí";
        }

    promedio = (promedio / num).toFixed(2);

    p.innerHTML = `<p>Promedio: ${promedio}</p>`;

    p2.innerHTML = `<p>Mayor: ${ma}, Índice: ${a}</p>`;
    p3.innerHTML = `<p>Menor: ${me}, Índice: ${b}</p>`;

    p4.innerHTML = `<p>Porcentaje de Pares: ${porcentajeEven}</p>`;
    p5.innerHTML = `<p>Porcentaje de Impares: ${porcentajeOdd}</p>`;

    p6.innerHTML = `<p>Simétrico: ${cons}</p>`;
    console.log(arr)
}

const gen = document.getElementById('gen');
    gen.addEventListener('click', function() {
        let largo = document.getElementById('largo').value;
        random(largo);
});

function ejecutarFunciones() {
        mostrarArray();
        promedio();
        pares();
        mayor();
}