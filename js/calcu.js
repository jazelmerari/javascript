document.getElementById("limpiar").addEventListener("click", function () {
    const tablaContenedor = document.getElementById("tablaContenedor");
    tablaContenedor.innerHTML = '';
});

document.getElementById("calcular").addEventListener("click", function () {
    const tabla = document.getElementById("tabla");
    const tablaContenedor = document.getElementById("tablaContenedor");
    const selectedTable = parseInt(tabla.value);

    tablaContenedor.innerHTML = '';
    for (let i = 1; i <= 10; i++) {
        const resultado = selectedTable * i;
        const cell = document.createElement("div");
        cell.classList.add("table-cell");

        const xImage = document.createElement("img");
        xImage.classList.add("numero");
        xImage.src = "../img/" + selectedTable + ".png";
        cell.appendChild(xImage);

        const multiplyImage = document.createElement("img");
        multiplyImage.classList.add("numero");
        multiplyImage.src = "../img/x.png";
        cell.appendChild(multiplyImage);

        for (let digit of i.toString()) {
            const digitImage = document.createElement("img");
            digitImage.classList.add("numero"); 
            digitImage.src = '../img/' + digit + ".png";
            cell.appendChild(digitImage);
        }

        const equalImage = document.createElement("img");
        equalImage.classList.add("numero"); 
        equalImage.src = "../img/=.png";
        cell.appendChild(equalImage);

        for (let digit of resultado.toString()) {
            const digitImage = document.createElement("img");
            digitImage.classList.add("numero"); 
            digitImage.src = '../img/' + digit + ".png";
            cell.appendChild(digitImage);
        }

        tablaContenedor.appendChild(cell);
    }
});
