const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener('click', function () {
    const valorAltura = parseFloat(document.getElementById('altura').value);
    const valorPeso = parseFloat(document.getElementById('peso').value);
    const valorEdad = parseFloat(document.getElementById('edad').value);
    const resultadoIMC = document.getElementById('resimc');

    if (valorAltura > 0 && valorPeso > 0) {
        const imc = valorPeso / (valorAltura ** 2);
        const imcFixed = imc.toFixed(2);
        resultadoIMC.innerHTML = `<p>Tu IMC es de ${imcFixed}</p>`;
        
        const generoRadios = document.querySelectorAll('input[name="sexo"]:checked');
        if (generoRadios.length > 0) {
            const genero = generoRadios[0].value;
            if (genero === 'h') {
                cargaImagen(imc, valorEdad, valorPeso); 
            } else {
                cargaImagenm(imc, valorEdad, valorPeso); 
            }
        }
}});

const btnLimpiar = document.getElementById('btnLimpiar');
btnLimpiar.addEventListener('click', function () {
    document.getElementById('altura').value = '';
    document.getElementById('peso').value = '';
    document.getElementById('edad').value = '';
    document.getElementById('resimc').innerHTML = '';
    document.getElementById('imagen').src = '';
    document.querySelectorAll('input[name="sexo"]').forEach((radio), function (){
        radio.checked = false;
    });
    document.getElementById('imc2').innerHTML = '';
});

function cargaImagen(imc, edad, valorPeso) { 
    let img = document.getElementById('imagen');
    if (imc < 18.5) {
        img.src = `../img/01H.png`;
    } else if (imc > 18.5 && imc < 24.9) {
        img.src = `../img/02H.png`;
    } else if (imc > 24.9 && imc < 29.9) {
        img.src = `../img/03H.png`;
    } else if (imc > 29.9 && imc < 34.9) {
        img.src = `../img/04H.png`;
    } else if (imc > 35 && imc < 39.9) {
        img.src = `../img/05H.png`;
    } else {
        img.src = `../img/06H.png`;
    }
    
    let imc2 = document.getElementById('imc2');
    if (edad >= 10 && edad <= 18) {
        const cals = ((17.686 * valorPeso) + 658).toFixed(2);
        imc2.innerHTML = `<p>Cantidad de calorías por día: ${cals}</p>`;
    } else if (edad >= 18 && edad <= 30) {
        const cals = ((15.057 * valorPeso) + 692.2).toFixed(2);
        imc2.innerHTML = `<p>Cantidad de calorías por día: ${cals}</p>`;
    } else if (edad >= 30 && edad <= 60) {
        const cals = ((11.472 * valorPeso) + 893.1).toFixed(2);
        imc2.innerHTML = `<p>Cantidad de calorías por día: ${cals}</p>`;
    } else {
        const cals = ((11.711 * valorPeso) + 587.7).toFixed(2);
        imc2.innerHTML = `<p>Cantidad de calorías por día: ${cals}</p>`;
    }
}

function cargaImagenm(imc, edad, valorPeso) { 
    let img = document.getElementById('imagen');
    if (imc < 18.5) {
        img.src = `../img/01M.png`;
    } else if (imc > 18.5 && imc < 24.9) {
        img.src = `../img/02M.png`;
    } else if (imc > 24.9 && imc < 29.9) {
        img.src = `../img/03M.png`;
    } else if (imc > 29.9 && imc < 34.9) {
        img.src = `../img/04M.png`;
    } else if (imc > 35 && imc < 39.9) {
        img.src = `../img/05M.png`;
    } else {
        img.src = `../img/06M.png`;
    }

    let imc2 = document.getElementById('imc2');
    if (edad >= 10 && edad <= 18) {
        const cals = ((13.384 * valorPeso) + 692.6).toFixed(2);
        imc2.innerHTML = `<p>Cantidad de calorías por día: ${cals}</p>`;
    } else if (edad >= 18 && edad <= 30) {
        const cals = ((14.818 * valorPeso) + 486.6).toFixed(2);
        imc2.innerHTML = `<p>Cantidad de calorías por día: ${cals}</p>`;
    } else if (edad >= 30 && edad <= 60) {
        const cals = ((8.126 * valorPeso) + 845.6).toFixed(2);
        imc2.innerHTML = `<p>Cantidad de calorías por día: ${cals}</p>`;
    } else {
        const cals = ((9.082 * valorPeso) + 658.5).toFixed(2);
        imc2.innerHTML = `<p>Cantidad de calorías por día: ${cals}</p>`;
    }
}